import numpy as np
from scipy import signal

LayerIndex = 0;

class Layer:
    def __init__(self):
        self.input = None
        self.output = None

        global LayerIndex
        self.index = LayerIndex
        LayerIndex += 1
    
    def forward(self, input):
        pass

    def backward(self, output_gradient, learning_rate):
        pass

    def save(self, file):
        pass
    
    def load(self, file):
        pass

#connect i input neurons to j output neurons
#w[j][i]
class Dense(Layer):
    def __init__(self, input_size, output_size):
        Layer.__init__(self)
        self.input_size = input_size
        self.output_size = output_size
        self.weights = np.random.randn(output_size, input_size)
        self.bias = np.random.randn(output_size, 1)

    def forward(self, input):
        self.input = input
        return np.dot(self.weights, self.input) + self.bias

    def backward(self, output_gradient, learning_rate):
        weights_gradient = np.dot(output_gradient, self.input.T)
        input_gradient = np.dot(self.weights.T, output_gradient)

        self.weights -= learning_rate * weights_gradient
        self.bias -= learning_rate * output_gradient
        return input_gradient

    def load(self, file):
        weightsBytesLength = int.from_bytes(file.read(4), "big")
        weights = np.frombuffer(file.read(weightsBytesLength)).reshape(self.output_size, self.input_size)
        self.weights = np.copy(weights)

        biasBytesLength = int.from_bytes(file.read(4), "big")
        bias = np.frombuffer(file.read(biasBytesLength)).reshape(self.output_size, 1)
        self.bias = np.copy(bias)

    def save(self, file):
        weightsBytes = bytearray(self.weights.tobytes())
        biasBytes = bytearray(self.bias.tobytes())

        file.write(len(weightsBytes).to_bytes(4, "big"))
        file.write(weightsBytes)
        file.write(len(biasBytes).to_bytes(4, "big"))
        file.write(biasBytes)

class Convolutional(Layer):
    def __init__(self, input_shape, kernel_size, depth):
        input_depth, input_height, input_width = input_shape
        self.depth = depth
        self.input_shape = input_shape
        self.input_depth = input_depth
        self.output_shape = (depth, input_height - kernel_size + 1, input_width - kernel_size + 1)
        self.kernels_shape = (depth, input_depth, kernel_size, kernel_size)
        self.kernels = np.random.randn(*self.kernels_shape)
        self.biases = np.random.randn(*self.output_shape)

    def forward(self, input):
        self.input = input
        self.output = np.copy(self.biases)
        for i in range(self.depth):
            for j in range(self.input_depth):
                self.output[i] += signal.correlate2d(self.input[j], self.kernels[i, j], "valid")
        return self.output

    def backward(self, output_gradient, learning_rate):
        kernels_gradient = np.zeros(self.kernels_shape)

        for i in range(self.depth):
            for j in range(self.input_depth):
                kernels_gradient[i, j] = signal.correlate2d(self.input[j], output_gradient[i], "valid")

        self.kernels -= learning_rate * kernels_gradient
        self.biases -= learning_rate * output_gradient

    def load(self, file):
        kernelsBytesLength = int.from_bytes(file.read(4), "big")
        kernels = np.frombuffer(file.read(kernelsBytesLength)).reshape(*self.kernels_shape)
        self.kernels = np.copy(kernels)

        biasBytesLength = int.from_bytes(file.read(4), "big")
        biases = np.frombuffer(file.read(biasBytesLength)).reshape(*self.output_shape)
        self.biases = np.copy(biases)

    def save(self, file):
        kernelsBytes = bytearray(self.kernels.tobytes())
        biasBytes = bytearray(self.biases.tobytes())

        file.write(len(kernelsBytes).to_bytes(4, "big"))
        file.write(kernelsBytes)
        file.write(len(biasBytes).to_bytes(4, "big"))
        file.write(biasBytes)


class Reshape(Layer):
    def __init__(self, input_shape, output_shape):
        self.input_shape = input_shape
        self.output_shape = output_shape

    def forward(self, input):
        return np.reshape(input, self.output_shape)

    def backward(self, output_gradient, learning_rate):
        return np.reshape(output_gradient, self.input_shape)

    def load(self, file):
        return

    def save(self, file):
        return

#Passes layer thru some activation function
class Activation(Layer):
    def __init__(self, activation, activation_prime):
        self.activation = activation
        self.activation_prime = activation_prime

    def forward(self, input):
        self.input = input
        return self.activation(self.input)

    def backward(self, output_gradient, learning_rate):
        return np.multiply(output_gradient, self.activation_prime(self.input))

    def load(self, file):
        return

    def save(self, file):
        return

class Tanh(Activation):
    def __init__(self):
        def tanh(x):
            return np.tanh(x)

        def tanh_prime(x):
            return 1 - np.tanh(x) ** 2

        super().__init__(tanh, tanh_prime)

class Sigmoid(Activation):
    def __init__(self):
        def sigmoid(x):
            return 1 / (1 + np.exp(-x))

        def sigmoid_prime(x):
            s = sigmoid(x)
            return s * (1 - s)

        super().__init__(sigmoid, sigmoid_prime)

def mse(y_true, y_pred):
    return np.mean(np.power(y_true - y_pred, 2))

def mse_prime(y_true, y_pred):
    return 2 * (y_pred - y_true) / np.size(y_true)

def binary_cross_entropy(y_true, y_pred):
    return np.mean(-y_true * np.log(y_pred) - (1 - y_true) * np.log(1 - y_pred))

def binary_cross_entropy_prime(y_true, y_pred):
    return ((1 - y_true) / (1 - y_pred) - y_true / y_pred) / np.size(y_true)

def predict(network, input):
    output = input
    for layer in network:
        output = layer.forward(output)
    return output

def train(network, loss, loss_prime, x_train, y_train, epochs = 1000, learning_rate = 0.01, verbose = True):
    for e in range(epochs):
        error = 0
        for x, y in zip(x_train, y_train):
            output = predict(network, x)
            error += loss(y, output)
            grad = loss_prime(y, output)
            for layer in reversed(network):
                grad = layer.backward(grad, learning_rate)

        error /= len(x_train)
        if verbose:
            print(f"{e + 1}/{epochs}, error={error}")

def save(network, filename):
    file = open(filename, "wb")
    for layer in network:
        layer.save(file)
    file.close()

def load(network, filename):
    file = open(filename, "rb")
    for layer in network:
        layer.load(file)
    file.close()