import matplotlib.pyplot as plt
import numpy as np

def read_label_image_pairs(imagesPath, labelsPath, images = [], labels = [], labelPadding = 0, rotate = False):
    imagesFile = open(imagesPath, "rb")
    labelsFile = open(labelsPath, "rb")

    imagesFile.seek(4);
    buf = imagesFile.read(4);
    imageCount = np.frombuffer(buf, dtype=np.uint32).newbyteorder().astype(np.int64)[0]

    for x in range(imageCount):
        images.append(read_image_wf(imagesFile, x, 28, rotate))
        labels.append(read_label_wf(labelsFile, x) + labelPadding)

    imagesFile.close()
    labelsFile.close()

    return images, labels

def read_image_wf(imagesFile, index, imageSize = 28, rotate = False):
    realImageSize = imageSize * imageSize

    imagesFile.seek(16 + (index * realImageSize))
    buf = imagesFile.read(realImageSize)
    data = np.frombuffer(buf, dtype=np.uint8).astype(np.float32)

    if rotate:
        data = data.reshape(imageSize, imageSize)
        data = np.rot90(data, k=3)

    data = data.reshape(1, imageSize, imageSize, 1)

    return data

def read_image(imagesPath, index, imageSize = 28):
    imagesFile = open(imagesPath, "rb")

    data = read_image_wf(imagesFile, index, imageSize);

    imagesFile.close()

    return data

def read_label_wf(labelsFile, index):
    labelsFile.seek(8 + index)
    buf = labelsFile.read(1)
    labels = np.frombuffer(buf, dtype=np.uint8).astype(np.int64)

    return labels[0]

def read_label(labelsPath, index):
    labelsFile = open(labelsPath, "rb")

    label = read_label_wf(labelsFile, index)

    labelsFile.close()

    return label

def print_image(img, label):
    if label >= 10:
        print(f"Printing image with label: {chr(label + 55)}")
    else:
        print(f"Printing image with label: {label}")

    image = np.asarray(img).squeeze()
    plt.imshow(image)
    plt.show()