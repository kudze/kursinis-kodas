import mnist_util as mnist
import cnn_util as cnn
import numpy as np
from keras.utils import np_utils
import random

print("Reading training images...")
train_img, train_lbl = mnist.read_label_image_pairs("train-images-idx3-ubyte", "train-labels-idx1-ubyte", [], [], 0, False)
train_img, train_lbl = mnist.read_label_image_pairs("emnist-letters-train-images-idx3-ubyte", "emnist-letters-train-labels-idx1-ubyte", train_img, train_lbl, 9, True) # we add 10 to label of characters

print("Reading test images...")
test_img, test_lbl = mnist.read_label_image_pairs("t10k-images-idx3-ubyte", "t10k-labels-idx1-ubyte", [], [], 0, False)
test_img, test_lbl = mnist.read_label_image_pairs("emnist-letters-test-images-idx3-ubyte", "emnist-letters-test-labels-idx1-ubyte", test_img, test_lbl, 9, True) # we add 10 to label of characters


def preprocess_data(img, lbl):
    x = []

    imgCount = len(img);
    for i in range(imgCount):
        x.append([np.reshape(img[i], (28,28) ).astype("float32") / 255])

    y = np_utils.to_categorical(lbl).reshape(len(img), 36, 1)

    return x, y

def shuffle_data(x_t, y_t, possible_limit):

    data = []
    for idx in range(len(x_t)):
        data.append({"x": x_t[idx], "y": y_t[idx]})

    random.shuffle(data)

    limit = min(len(x_t), possible_limit)

    nx_t = []
    ny_t = []
    for idx in range(limit):
        nx_t.insert(idx, data[idx]["x"])
        ny_t.insert(idx, data[idx]["y"])

    return nx_t, ny_t

x_train, y_train = preprocess_data(train_img, train_lbl)
x_test, y_test = preprocess_data(test_img, test_lbl)

test_mode = False

if test_mode == False:
    train_limit = len(x_train);
    test_limit = len(x_test);
else:
    train_limit = 1000
    test_limit = 100

x_train, y_train = shuffle_data(x_train, y_train, train_limit)
x_test, y_test = shuffle_data(x_test, y_test, test_limit)

print(f"Train size: {len(x_train)}")
print(f"Test size: {len(x_test)}")

#To test we can print image.
#mnist.print_image(test_img[20000], test_lbl[20000])
#quit();

# neural network

# 95% digits
network = [
    cnn.Convolutional((1, 28, 28), 3, 10),
    cnn.Reshape((10, 26, 26), (10 * 26 * 26, 1)),
    cnn.Sigmoid(),
    cnn.Dense(10 * 26 * 26, 200),
    cnn.Sigmoid(),
    cnn.Dense(200, 10 + 26),
    cnn.Sigmoid()
]

print("")
print(f"***")
print(f"*** Sveiki, help - komandų sąrašas")
print(f"***")
print("")

def cmd_help():
    print("* Pagalbos meniu:")
    print("* help - komandu sarasas")
    print("* save - Išsaugo tinklo parametrus i faila")
    print("* load - Užkrauna tinklo parametrus iš failo")
    print("* train - Treniruoja tinkla su treniravimo duomenų aibe.")
    print("* test - Testuoja tinkla su testavimo duomenų aibe.")
    print("* quit - Baigia darba.")

def cmd_train():
    num = input("Kiek epochu: ")
    num = int(num)

    print("Mokinamas tinklas, gali užtrukti...")
    cnn.train(
        network,
        cnn.binary_cross_entropy,
        cnn.binary_cross_entropy_prime,
        x_train,
        y_train,
        epochs=num,
        learning_rate=0.1,
        verbose=True
    )

def cmd_test():
    print("Testuojamas tinklas...")

    correct = 0
    for i in range(test_limit):
        output = cnn.predict(network, x_test[i])
        prediction = np.argmax(output)
        label = np.argmax(y_test[i])

        if prediction == label:
            correct += 1

        predictionToPrint = prediction
        labelToPrint = label
        if(predictionToPrint >= 10): 
            predictionToPrint = chr(predictionToPrint + 55)
        if(labelToPrint >= 10): 
            labelToPrint = chr(labelToPrint + 55)

        print(f"input id: {i}, output: {predictionToPrint}, answer: {labelToPrint}")

    print(f"Correct/Total: {correct}/{test_limit}")
    print(f"Padalinus: {correct/test_limit}")

def cmd_save():
    filename = input("Enter filename: ")

    print(f"Saving trainable parameters into: {filename}")
    cnn.save(network, filename)

def cmd_load():
    filename = input("Enter filename: ")

    print(f"Loading trainable parameterse from: {filename}")
    cnn.load(network, filename)

def cmd_quit():
    print("Viso gero!")
    quit()

cmds = {};
cmds['help'] = cmd_help
cmds['train'] = cmd_train
cmds['test'] = cmd_test
cmds['load'] = cmd_load
cmds['save'] = cmd_save
cmds['quit'] = cmd_quit

while True:
    cmd = input("Command: ")

    print()
    if cmd in cmds:
        cmds[cmd]()
    else:
        print("Tokios komandos nėra!")
    print()

